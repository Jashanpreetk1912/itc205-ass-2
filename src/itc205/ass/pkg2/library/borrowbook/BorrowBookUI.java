package library.borrowbook;
import java.util.Scanner;


public class BorrowBookUI {
	
	public static enum UiState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };

	private BorrowBookControl Control;
	private Scanner Input;
	private UiState State;

	
	public BorrowBookUI(BorrowBookControl control) {
		this.control = control;
		Input = new Scanner(System.in);
		State = UiState.INITIALISED;
		control.setUi(this);
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return Input.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}
	
			
	public void SetState(UiState state) {
		this.State = state;
	}

	
	public void run() {
		output("Borrow Book Use Case UI\n");
		
		while (true) {
			
			switch (State) {			
			
			case CANCELLED:
				output("Borrowing Cancelled");
				return;

				
			case READY:
				String MEM_STR = input("Swipe member card (press <enter> to cancel): ");
				if (MEM_STR.length() == 0) {
					control.cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(MEM_STR).intValue();
					control.swiped(memberId);
				}
				catch (NumberFormatException e) {
					output("Invalid Member Id");
				}
				break;

				
			case RESTRICTED:
				input("Press <any key> to cancel");
				control.cancel();
				break;
			
				
			case SCANNING:
				String BookStringInput = input("Scan Book (<enter> completes): ");
				if (BookStringInput.length() == 0) {
					control.complete();
					break;
				}
				try {
					int bid = Integer.valueOf(BookStringInput).intValue();
					control.scanned(bid);
					
				} catch (NumberFormatException e) {
					output("Invalid Book Id");
				} 
				break;
					
				
			case FINALISING:
				String ans = input("Commit loans? (Y/N): ");
				if (ans.toUpperCase().equals("N")) {
					control.cancel();
					
				} else {
					control.commitLoans();
					input("Press <any key> to complete ");
				}
				break;
				
				
			case COMPLETED:
				output("Borrowing Completed");
				return;
	
				
			default:
				output("Unhandled state");
				throw new RuntimeException("BorrowBookUI : unhandled state :" + State);			
			}
		}		
	}


	public void display(Object object) {
		output(object);		
	}


}
