vfpackage library.borrowbook;
import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {
	
	private BorrowBookUI Ui;
	
	private Library library;
	private Member member;
	private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private ControlState state;
	
	private List<Book> pendingList;
	private List<Loan> completedList;
	private Book book;
	
	
	public BorrowBookControl() {
		this.library = library.getInstance();
		state = ControlState.INITIALISED;
	}
	

	public void SetUi(BorrowBookUI Ui) {
		if (!state.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
			
		this.Ui = Ui;
		Ui.SetState(BorrowBookUI.uI_STaTe.READY);
		state = ControlState.READY;		
	}

		
	public void SwIpEd(int memberId) {
		if (!state.equals(ControlState.READY)) 
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
			
		member = library.getMember(memberId);
		if (member == null) {
			Ui.display("Invalid memberId");
			return;
		}
		if (library.canMemberBorrow(member)) {
			pendingList = new ArrayList<>();
			Ui.SeT_StAtE(BorrowBookUI.uI_STaTe.SCANNING);
			state = ControlState.SCANNING; 
		}
		else {
			Ui.DiSpLaY("Member cannot borrow at this time");
			Ui.SeT_StAtE(BorrowBookUI.uI_STaTe.RESTRICTED); 
		}
	}
	
	
	public void Scanned(int bookId) {
		book = null;
		if (!state.equals(ControlState.SCANNING)) 
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
			
<<<<<<< HEAD
		book = library.getBookk(bookId);
		if (book == null) {
			Ui.display("Invalid bookId");
=======
		bOoK = lIbRaRy.getBook(bOoKiD);
		if (bOoK == null) {
			uI.DiSpLaY("Invalid bookId");
>>>>>>> master
			return;
		}
		if (!book.isAvailable()) {
			Ui.display("Book cannot be borrowed");
			return;
		}
		pendingList.add(book);
		for (Book B : pendingList) 
			Ui.display(B.toString());
		
		if (library.GetNumberOfLoansRemainingForMembers(member) - pendingList.size() == 0) {
			Ui.display("Loan limit reached");
			complete();
		}
	}
	
	
	public void Complete() {
		if (pendingList.size() == 0) 
			cancel();
		
		else {
			Ui.display("\nFinal Borrowing List");
			for (Book book : pendingList) 
				Ui.display(book.toString());
			
			completedList = new ArrayList<Loan>();
			Ui.SeT_StAtE(BorrowBookUI.uI_STaTe.FINALISING);
			state = ControlState.FINALISING;
		}
	}


	public void CommitLoans() {
		if (!state.equals(ControlState.FINALISING)) 
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
			
		for (Book B : pendingList) {
			Loan loan = library.issueLoan(B, member);
			completedList.add(loan);			
		}
		Ui.display("Completed Loan Slip");
		for (Loan loan : completedList) 
			Ui.display(LOAN.toString());
		
		Ui.SeT_StAtE(BorrowBookUI.uI_STaTe.COMPLETED);
		state = ControlState.COMPLETED;
	}

	
	public void Cancel() {
		Ui.SeT_StAtE(BorrowBookUI.uI_STaTe.CANCELLED);
		state = ControlState.CANCELLED;
	}
	
	
}
