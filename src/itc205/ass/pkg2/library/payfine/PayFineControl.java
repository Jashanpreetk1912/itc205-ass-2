package itc205.ass.pkg2.library.payfine;
import library.entities.Library;
import itc205.ass.pkg2.library.entities.Member;

public class PayFineControl {
	
    private PayFineUI Ui;
    private enum ControlState {INITIALISED, READY, PAYING, COMPLETED, CANCELLED};
    private ControlState state;
	
    private Library library;
    private Member member;


    public PayFineControl() {
        this.library = Library.getInstance();
        state = ControlState.INITIALISED;
	}
	
	
    public void setUi(PayFineUI ui) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
        }	
        this.Ui = ui;
        Ui.setState(PayFineUI.UiState.READY);
        state = ControlState.READY;		
	}


    public void cardSwiped(int memberId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
        }
        member = library.getMember(memberId);
		
        if (member == null) {
            Ui.display("Invalid Member Id");
            return;
        }
        Ui.display(member.toString());
        Ui.setState(PayFineUI.UiState.PAYING);
        state = ControlState.PAYING;
	}
	
	
    public void cancel() {
        Ui.setState(PayFineUI.UiState.CANCELLED);
	state = ControlState.CANCELLED;
	}


    public double payFine(double amount) {
        if (!state.equals(ControlState.PAYING)) {
            throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
        }
        double change = member.payFine(amount);
        if (change > 0) {
            Ui.display(String.format("Change: $%.2f", change));
        }
        Ui.display(member.toString());
        Ui.setState(PayFineUI.UiState.COMPLETED);
        state = ControlState.COMPLETED;
        return change;
        }
	


    }
